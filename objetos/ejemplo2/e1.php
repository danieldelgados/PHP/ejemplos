<?php
function __autoload($nombre_clase) {//funcion especial le paso el argumento (autocarga de clases)
    include $nombre_clase . '.php';
}
?>
<!DOCTYPE html>
<!--
Ejemplo 2016
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $objeto=new Vector([1,2,45,1,2,3,44]);//instanciar un objeto
            echo "La moda es ". $objeto->getModa();
            echo"<br>";
            echo "El valor maximo es ". $objeto->getMaximo();
            echo"<br>";
            echo "Los valores introducidos son ". $objeto->getCadena();
        ?>
    </body>
</html>
