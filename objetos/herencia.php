<?php
function __autoload($nombre_clase) {
    include "herencia/" . $nombre_clase . '.php';
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        $objeto1 = new Ambulancia();
        $objeto1->pintar();
        // no puedo hacer esto
        //echo $objeto1->nombre;
        //si puedo hacer esto
        $objeto1->getNombre();
        $objeto1->pintarPadre();

        ?>
    </body>
</html>
