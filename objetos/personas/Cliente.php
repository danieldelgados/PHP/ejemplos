<?php

/**
 * Description of Cliente
 *
 * @author ramon
 */
class Cliente extends Persona{
    private $telefonoContacto;
    public function mostrar() {
        echo "Soy un cliente y mi telefono es $this->telefonoContacto";
    }
    public function  mostrarPadre(){
        parent::mostrar();// asi llamamos al padre cunado no es estatico
    }
    public function __construct($argumentos=[]) {
        $this->telefonoContacto=isset($argumentos["telefono"])?$argumentos["telefono"]:"";
        //quiero ejecutar el constructor del padre
        parent::__construct($argumentos);// si no llamas al constructor del padre no tendrías ni nombre ni edad.
        
//        $b=($a==10)?23:30; uso de operadores ternarios
    }
}
