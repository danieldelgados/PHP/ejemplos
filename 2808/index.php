<!DOCTYPE html>
<?php
    include 'controlador.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css" media="screen">
            
            .errores{
                color: red;
                font-style: italic;
                font-weight: bold;
                margin: 1px 200px;              
            }
            .titulo{
                margin: 20px 200px;
                text-decoration: underline;
                font-family: calibri;
                font-size: 2em;
                
            }
            
        </style>
    </head>
    <body> 
        
        <div class="wrap">
            <?php
                if($formulario){
                     include 'formulario.php';
                }else{
                    include 'resultado.php';
                };
            ?>
        </div>
       
    </body>
       
</html>
