<div class="errores">
<?php
    //var_dump($modelo["errores"]);
    
    if(is_array($modelo["errores"])){
        echo "<ul>";
        foreach ($modelo["errores"] as $valor){
            echo "<li>$valor</li>";
        }
        echo "</ul>";
    }else{
        echo $modelo["errores"];
    }
?>
</div>

<form method="get">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" value="<?= $modelo["nombre"]["valor"]; ?>" placeholder="Escribe tu nombre">
        <span class="error"> <?= $modelo["nombre"]["error"] ?></span>
    </div>
    <div>
        <label for="nombre">Poblacion</label>
        <select name="poblacion" id="poblacion">
            <option value="">Selecciona la poblacion</option>
            <?php
                if($modelo["poblacion"]["valor"]=="torrelavega"){
                    echo '<option value="torrelavega" selected>Torrelavega</option>';
                }else{
                    echo '<option value="torrelavega">Torrelavega</option>';
                }
            ?>
            <?php
                $seleccionado=($modelo["poblacion"]["valor"]=="laredo")?"selected":"";
            ?>
            <option value="laredo" <?= $seleccionado; ?>>Laredo</option>
            
            <option value="potes" <?= ($modelo["poblacion"]["valor"]=="potes")?"selected":""; ?>>Potes</option>
        </select> 
           <?php echo '<span class="error">' . $modelo["poblacion"]["error"] . '</span>';?>
            
    </div>
    <div>
        <label for="edad">Edad</label>
        <input type="number" name="edad" id="edad" value="<?= $modelo["edad"]["valor"]; ?>" placeholder="Escribe la edad">
        <?php echo '<span class="error">' . $modelo["edad"]["error"] . '</span>';?>
            
    </div>
    <div>
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" id="telefono" value="<?= $modelo["telefono"]["valor"]; ?>" placeholder="escribe tu telefono">
        <?php echo '<span class="error">' . $modelo["telefono"]["error"] . '</span>';?>
            
    </div>
    <button>Enviar</button>
</form>

