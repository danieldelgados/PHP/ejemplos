
<form method="get">
    <div>
        <label form="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre"  placeholder="Ingresa tu nombre" value="<?= $modelo["nombre"]; ?>">                         
    </div></br>
    <div>
        <label form="poblacion">Población</label>
        <select name="poblacion" id="poblacion">
            <option value="">Elige una población</option>
            <?php
                if($modelo['poblacion']=='Torrelavega'){
                  echo  '<option value="Santander" selected>Santander</option>';
                } else {
                    echo  '<option value="Santander">Santander</option>';
                }
            ?>
            <?php
                $seleccionado=($modelo["poblacion"]=="Laredo")?"selected":"";
            ?>
            <option value="Laredo" <?php$seleccionado;?>>Laredo</option>
            <option value="Maliaño" <?php ($modelo["poblacion"]=="Maliaño")?"selected":"";?> >Maliaño</option>
            <option value="Torrelavega" <?php ($modelo["poblacion"]=="Torrelavega")?"selected":"";?>>Torrelavega</option>        
            
        </select>  
    </div></br>
    <div>
        <label form="edad">Edad</label>
        <input type="number" name="edad" id="edad" value="<?= $modelo['edad'];?>" placeholder="Ingresa tu edad">  
    </div></br>
    <div>
        <label form="telefono">Telefono</label>
        <input type="tel" name="telefono" id="edad" value="<?= $modelo["telefono"]; ?>" placeholder="Ingresa tu telefono">  
    </div></br>
    <div>
        <button>Listar</button>
    </div></br> 
    <div class="errores" >
    <?php
        if(is_array($modelo["errores"])){
            echo '<ul>';
            foreach ($modelo["errores"] as $valor) {
                echo "<li>$valor</li>";            
            }
            echo '</ul>';
        }else{
            echo $modelo['errores'];
        };
    ?>
    </div>
</form>

