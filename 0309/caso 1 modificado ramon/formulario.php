<div>
<?php
    //var_dump($modelo["errores"]);
    echo "<ul>";
    foreach($errores as $key =>$value){
        echo "<li> $key: $value </li>";
    }
    echo "</ul>";
?>
</div>

<form method="get">
    <?php
       dibujarControl($campos["nombre"],$errores,"nombre");
       dibujarControl($campos["telefono"],$errores,"telefono");
       dibujarControl($campos["edad"],$errores,"edad");
       dibujarControl($campos["poblacion"],$errores,"poblacion");
       
    ?>   
    <div>
        <label for="nombre">Poblacion</label>
        <select name="poblacion" id="poblacion">
            <option value="">Selecciona la poblacion</option>
            <?php
                if($modelo["poblacion"]=="torrelavega"){
                    echo '<option value="torrelavega" selected>Torrelavega</option>';
                }else{
                    echo '<option value="torrelavega">Torrelavega</option>';
                }
            ?>
            
            <?php
                $seleccionado=($modelo["poblacion"]=="laredo")?"selected":"";
            ?>
            <option value="laredo" <?= $seleccionado; ?>>Laredo</option>
            
            <option value="potes" <?= ($modelo["poblacion"]=="potes")?"selected":""; ?>>Potes</option>
        </select> 
    </div>
    
    <button>Enviar</button>
</form>

