<?php
    $campos=[
      "nombre"=>[
          "label"=>"Nombre Completo",
          "placeholder"=>"Escribe tu nombre",
          "value"=>"",
          "error"=>"El nombre no puede estra vacio",
          "type"=>"text"
      ],
      "edad"=>[
          "label"=>"Edad",
          "placeholder"=>"Escribe tu edad",
          "value"=>"",
          "error"=>"La edad no puede estar vacia",
          "type"=>"number"          
      ],
      "telefono"=>[
          "label"=>"Telefono",
          "placeholder"=>"Escribe tu telefono",
          "value"=>"",
          "error"=>"El telefono no puede estar vacio",
          "type"=>"texto"
      ],
      "poblacion"=>[
          "label"=>"Poblacion",
          "placeholder"=>"Selecciona tu población",
          "value"=>"",
          "error"=>"Debes seleccionar una poblacion",
          "type"=>"select",
          "options"=>["Torrelavega","Potes","Laredo"]              
      ]  
    ];
