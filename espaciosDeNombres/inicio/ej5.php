<?php
namespace uno;

function sumar() {
    return __NAMESPACE__ . " sumar_uno";
}

function restar() {
    return __NAMESPACE__ . " restar_uno";
}

namespace dos;

function sumar() {
    return __NAMESPACE__ .  " sumar_dos";
}

function restar() {
    return __NAMESPACE__ . " restar_dos";
}

namespace dos\primero;

function multiplicar(){
    return __NAMESPACE__ . " multiplicar_dos_primero";
}



?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
    echo multiplicar() . "<br>";
    //echo sumar() . "<br>"; //error en este espacio de nombres no existe
    namespace dos;
    echo sumar() . "<br>";
    //echo multiplicar() . "<br>"; //error en este espacio de nombres no existe
    
    use \dos\primero as alias;
    
    //echo multiplicar() . "<br>"; //error en este espacio de nombres no existe
    echo alias\multiplicar() . "<br>";
?>
    </body>
</html>
