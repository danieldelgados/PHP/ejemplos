<?php
    include "Ramon.php";
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // namespace alumno; // error
        alumno\Ramon::hola();
        alumno\pelo\Color::rojo();
        
        use alumno\pelo;
        
        pelo\Color::rojo();
        
        use alumno\pelo\Color;
        
        Color::rojo();
        
        use alumno\Ramon;
        
        Ramon::hola();
        
        ?>
    </body>
</html>
