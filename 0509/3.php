<?php

$alumnos=[
    $alumno1=[
        "nombre"=>"Cris",
        "edad"=>"29"
    ],
    $alumno2=[
        "nombre"=>"David",
        "edad"=>"35"
    ],
     $alumno3=[
        "nombre"=>"Ivan",
        "edad"=>"25"
    ],
     $alumno4=[
        "nombre"=>"Dani",
        "edad"=>"31"
    ],
     $alumno5=[
        "nombre"=>"Vero",
        "edad"=>"35"
    ],
];

foreach($alumnos as $key=>$value){
    echo '<div>  Nombre:'.$value['nombre'].'<br>';
    echo 'Edad: ' .$value['edad'].'</div><br>';
    
}

$datos=[
    [
        "nombre"=>"Dani",
        "edad"=>"31"
    ],
    [
        "nombre"=>"Cris",
        "edad"=>"29"
    ],
    [
        "nombre"=>"David",
        "edad"=>"35"
    ],
    [
        "nombre"=>"Vero",
        "edad"=>"35"
    ],
    [
        "nombre"=>"Ivan",
        "edad"=>"25"
    ]
    
];

foreach ($datos as $registro){
    echo '<div>';
    foreach ($registro as $nombreCampo=>$valorCampo){
        echo "$nombreCampo: $valorCampo <br>";
    }
    echo '</div>';
}