<?php

class Cadena {

    private $valor;
    private $longitud;
    private $vocales;

    public function __construct($v) {
        $this->valor = $v;
    }

//    private function valor($v) {
//       $this-> valores 
//    }
    public function getValor() {
        return $this->valor;
    }

    private function setLongitud() {
        $this->longitud = strlen($this->valor);
    }

    public function getLongitud() {
        $this->setLongitud();
        return $this->longitud;
    }

    private function contarVocales() {
        $this->vocales = substr_count($this->valor, 'a') + substr_count($this->valor, 'e') + substr_count($this->valor, 'i') + 
                substr_count($this->valor, 'o') + substr_count($this->valor, 'u') + substr_count($this->valor, 'A') + substr_count($this->valor, 'E') + substr_count($this->valor, 'I') + 
                substr_count($this->valor, 'O') + substr_count($this->valor, 'U');
    }

    public function getVocales() {
        $this->contarVocales();
        return $this->vocales;
    }

}
