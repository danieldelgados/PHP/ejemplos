<?php
    function __autoload($nombre_clase) {//funcion especial le paso el argumento (autocarga de clases)
    include $nombre_clase . '.php';
}
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $a=new Cadena("Ejemplo");
            
            echo "El valor del objeto es ". $a->getValor();
            echo "<br>";        
            echo "La longitud es ". $a->getLongitud();
            echo "<br>"; 
            echo "El numero de vocales es ". $a->getVocales();
        ?>
    </body>
</html>
