<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
		<?php 
			function vocal($caracter){
				// $caracter=strtolower($caracter); esto es valido

				switch(strtolower($caracter)){//esto es mas correcto
					case 'a':
					case 'e':
					case 'i':
					case 'o':
					case 'u':
						return TRUE;
						break;
					default:
						return FALSE;
						break;

				}
				
			}
			$t="Supercalifragilisticoespialidoso";
			/*$array=str_split($t);//no hace falta pasar un string a un array*/

			for($c=0;$c<strlen($t);$c++){
				if(vocal($t[$c])){
					$t[$c]="-";
				}
			};

			echo $t;
			echo "</br>";
			/*para realiza pruebas*/ 
			print_r($t);
			echo "</br>";
			/* esto se utiliza para depurar*/
			// var_dump($t);
			/* */
			$v=[0,0,1,1,2];
			var_dump($v);


		?>
	
</body>
</html>